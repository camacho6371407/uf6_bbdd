# POO. PRÁCTICA UF6
## APLICACIÓN - PERSISTENCIA BBDD
### Yostin Arbey Camacho Velásquez  
Esta práctica es la continuación de la práctica de la UF5. El objetivo era utilizar  
la aplicación desarrollada anteriormente y hacer que la información guardada   
persistiera. Esto, con Docker para hacer uso de imágenes y contenedores. Ahora,  
con contenedores de MySql y phpMyAdmin, la aplición ya es capaz de almacenar toda  
la informacion en una base de datos y poder consultarla en cuanquier momento.   
  
Tambén se pidío una funcionalidad extra. En mi caso es la de pedir credenciales  
para ingresar a la aplicación. La documentación de cómo se realizó esto y otros  
archivos están en el contenido del repositorio.

Este repositorio contiene:
* Un documento donde se muestran los formularios, se prueba el funcionamiento  
del crud y un poco de la rebusteza del programa.
* El documento donde se explica la implementación de la validación de credenciales
* Un documento .sql que tiene las órdenes para crear la base de datos, sus tablas  
y algunos registros como ejemplo.
* Un YALM para crear los contenedores necesarios y su configuración. Dentro también hay un pequeño tutorial de cómo ponerlo en marcha.
* Finalmente, el proyecto java compactado.