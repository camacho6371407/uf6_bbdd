-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql:3306
-- Temps de generació: 22-05-2024 a les 17:52:55
-- Versió del servidor: 5.7.44
-- Versió de PHP: 8.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `UF6_Practica`
--
CREATE DATABASE IF NOT EXISTS `UF6_Practica` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `UF6_Practica`;

-- --------------------------------------------------------

--
-- Estructura de la taula `Alumnos`
--

CREATE TABLE `Alumnos` (
  `dni` varchar(9) NOT NULL,
  `usuario` varchar(25) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `fechaNacimiento` varchar(25) NOT NULL,
  `actsEscolares` varchar(100) NOT NULL,
  `precioMatricula` double NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `dniProfesor` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `Alumnos`
--

INSERT INTO `Alumnos` (`dni`, `usuario`, `nombre`, `apellidos`, `fechaNacimiento`, `actsEscolares`, `precioMatricula`, `descripcion`, `dniProfesor`) VALUES
('147258', 'ycamacho', 'Yostin', 'Camacho', '19 d’octubre de 2004', 'baloncesto, football', 436, '', '123456'),
('159753', 'lrojas', 'Laura', 'Rojas', '10 de maig de 2024', 'canto, manualidades', 142.2, '', '124563'),
('258369', 'dvisbal', 'David', 'Visbal', '17 d’octubre de 2013', 'canto', 420, '', '123456'),
('369147', 'fflorez', 'Fabián', 'Florez', '7 de juliol de 2017', '', 333, '', '142536'),
('369258', 'jtorres', 'Juan', 'Torres', '23 de maig de 2024', 'ajedrez', 213, '', '142536');

-- --------------------------------------------------------

--
-- Estructura de la taula `Profesores`
--

CREATE TABLE `Profesores` (
  `dni` varchar(9) NOT NULL,
  `usuario` varchar(25) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `fechaNacimiento` varchar(25) NOT NULL,
  `poblacion` varchar(30) NOT NULL,
  `cp` int(11) NOT NULL,
  `posicionado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `Profesores`
--

INSERT INTO `Profesores` (`dni`, `usuario`, `nombre`, `apellidos`, `fechaNacimiento`, `poblacion`, `cp`, `posicionado`) VALUES
('123456', 'apifarre', 'Toni', 'Pifarre', '1 de juny de 2004', 'Balaguer', 25600, 'S'),
('124563', 'yosarca', 'Arbey', 'Velásquez', '3 de maig de 2024', 'Lleida', 25001, 'N'),
('142536', 'pantaLeon', 'Panta', 'Leon', '16 de maig de 2013', 'Balaguer', 25600, 'S');

-- --------------------------------------------------------

--
-- Estructura de la taula `Usuarios`
--

CREATE TABLE `Usuarios` (
  `NomUser` varchar(25) NOT NULL,
  `password` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `Usuarios`
--

INSERT INTO `Usuarios` (`NomUser`, `password`) VALUES
('ycamacho', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92');

--
-- Índexs per a les taules bolcades
--

--
-- Índexs per a la taula `Alumnos`
--
ALTER TABLE `Alumnos`
  ADD PRIMARY KEY (`dni`),
  ADD KEY `DNIprofesor` (`dniProfesor`);

--
-- Índexs per a la taula `Profesores`
--
ALTER TABLE `Profesores`
  ADD PRIMARY KEY (`dni`);

--
-- Índexs per a la taula `Usuarios`
--
ALTER TABLE `Usuarios`
  ADD PRIMARY KEY (`NomUser`);

--
-- Restriccions per a les taules bolcades
--

--
-- Restriccions per a la taula `Alumnos`
--
ALTER TABLE `Alumnos`
  ADD CONSTRAINT `alumnos_ibfk_1` FOREIGN KEY (`dniProfesor`) REFERENCES `Profesores` (`dni`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
